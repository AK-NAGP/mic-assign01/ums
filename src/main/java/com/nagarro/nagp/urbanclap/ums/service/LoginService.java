package com.nagarro.nagp.urbanclap.ums.service;

import com.nagarro.nagp.urbanclap.ums.domain.model.request.LoginRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.LoginResponse;

public interface LoginService {

    LoginResponse loginUser(LoginRequest loginRequest);

}
