package com.nagarro.nagp.urbanclap.ums.domain.enums;

public enum UserType {
    ADMIN,
    END_USER,
    SERVICE_PROVIDER
}
