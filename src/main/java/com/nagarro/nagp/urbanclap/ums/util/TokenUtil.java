package com.nagarro.nagp.urbanclap.ums.util;

import java.util.Base64;

public class TokenUtil {

    public static String generateToken(String username, String password) {
        String finalBasicAuthString = username.trim() + ":" + password.trim();
        return new String(
                Base64
                        .getEncoder()
                        .encode(finalBasicAuthString.getBytes())
        );
    }
}
