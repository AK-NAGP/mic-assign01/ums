package com.nagarro.nagp.urbanclap.ums.domain.entity;

import com.nagarro.nagp.urbanclap.ums.domain.enums.UserType;
import lombok.Data;

import javax.persistence.*;

@Entity(name = "user")
@Data
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column(unique = true)
    private String username;

    @Column
    private String password;

    @Column
    private String email;

    @Column
    private String phone;

    @Column
    @Enumerated(EnumType.STRING)
    private UserType userType;

}
