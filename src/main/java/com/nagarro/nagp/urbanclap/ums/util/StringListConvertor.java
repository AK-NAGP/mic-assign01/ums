package com.nagarro.nagp.urbanclap.ums.util;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;

@Converter
public class StringListConvertor implements AttributeConverter<List<String>, String> {
    private static final Character SPLIT_CHAR = ';';

    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        return Joiner.on(SPLIT_CHAR).skipNulls().join(attribute);
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        return Splitter.on(SPLIT_CHAR).trimResults().omitEmptyStrings().splitToList(dbData);
    }

}
