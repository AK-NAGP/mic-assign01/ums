package com.nagarro.nagp.urbanclap.ums.service;

import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.ServiceProviderRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.SignupRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderListResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderResponse;

public interface ServiceProviderService {

    ServiceProviderListResponse getAllAvailableProviders(ServiceProviderRequest serviceProviderRequest);

    void signUpUser(UserEntity userEntity, ServiceProviderRequest serviceProvider);

    ServiceProviderResponse getUserById(String id);

    BaseResponse updateServiceProviderAvailability(String userId, Boolean isAvailable);
}
