package com.nagarro.nagp.urbanclap.ums.service.impl;

import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.enums.UserType;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.LoginRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.SignupRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.LoginResponse;
import com.nagarro.nagp.urbanclap.ums.respository.UserRepository;
import com.nagarro.nagp.urbanclap.ums.service.AddressService;
import com.nagarro.nagp.urbanclap.ums.service.LoginService;
import com.nagarro.nagp.urbanclap.ums.service.ServiceProviderService;
import com.nagarro.nagp.urbanclap.ums.service.SignupService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SignupServiceImpl implements SignupService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LoginService loginService;

    @Autowired
    private ServiceProviderService serviceProviderService;

    @Autowired
    private AddressService addressService;

    @Transactional
    @Override
    public LoginResponse signupUser(SignupRequest signupRequest) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(signupRequest, userEntity);

        this.userRepository.save(userEntity);

        //Extra Work for SERVICE_PROVIDER
        if (signupRequest.getUserType().equals(UserType.SERVICE_PROVIDER)) {
            serviceProviderService.signUpUser(userEntity, signupRequest.getServiceProvider());
        }

        //Extra Work for END_USER
        if (signupRequest.getUserType().equals(UserType.END_USER)) {
            addressService.saveUserAddress(userEntity, signupRequest.getAddress());
        }

        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(userEntity.getUsername());
        loginRequest.setPassword(userEntity.getPassword());

        return this.loginService.loginUser(loginRequest);
    }
}
