package com.nagarro.nagp.urbanclap.ums.controller;

import com.nagarro.nagp.urbanclap.ums.domain.model.request.ServiceProviderRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.UserResponse;
import com.nagarro.nagp.urbanclap.ums.service.ServiceProviderService;
import com.nagarro.nagp.urbanclap.ums.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("internal")
public class InternalController {

    @Autowired
    private ServiceProviderService providerService;

    @Autowired
    private UserService userService;

    @PostMapping("service-provider/update/{userId}")
    public ResponseEntity<BaseResponse> updateUser(
            @PathVariable("userId") String userId,
            @RequestBody ServiceProviderRequest serviceProviderRequest
    ) {
        return ResponseEntity.ok(providerService.updateServiceProviderAvailability(userId, serviceProviderRequest.getIsAvailable()));
    }


    @GetMapping("service-provider/{id}")
    public ResponseEntity<ServiceProviderResponse> getServiceProviderById(
            @PathVariable("id") String id
    ) {
        return ResponseEntity.ok(providerService.getUserById(id));
    }

    @GetMapping("user/{id}")
    public ResponseEntity<UserResponse> getUserById(
            @PathVariable("id") String id
    ) {
        return ResponseEntity.ok(userService.findUserByUserId(id));
    }


}
