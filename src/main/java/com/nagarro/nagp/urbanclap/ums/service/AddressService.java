package com.nagarro.nagp.urbanclap.ums.service;

import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.UserAddressRequest;

public interface AddressService {

    void saveUserAddress(UserEntity userEntity, UserAddressRequest userAddressRequest);
}
