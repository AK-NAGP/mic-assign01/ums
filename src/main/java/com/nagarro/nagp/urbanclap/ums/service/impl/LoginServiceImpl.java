package com.nagarro.nagp.urbanclap.ums.service.impl;

import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.LoginRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.LoginResponse;
import com.nagarro.nagp.urbanclap.ums.respository.UserRepository;
import com.nagarro.nagp.urbanclap.ums.service.LoginService;
import com.nagarro.nagp.urbanclap.ums.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public LoginResponse loginUser(LoginRequest loginRequest) {
        LoginResponse response;
        Optional<UserEntity> userEntityOptional = userRepository.findFirstByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword());
        if (userEntityOptional.isPresent()) {
            UserEntity userEntity = userEntityOptional.get();
            response = new LoginResponse("success", "User Login Successfully");
            String token = TokenUtil.generateToken(loginRequest.getUsername(), loginRequest.getPassword());

            response.setToken(token);
            response.setName(userEntity.getName());
            response.setUsername(loginRequest.getUsername());
        } else {
            response = new LoginResponse("failed", "Username/Password not valid");
        }

        return response;
    }
}
