package com.nagarro.nagp.urbanclap.ums.controller;

import com.nagarro.nagp.urbanclap.ums.domain.model.request.LoginRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.LoginResponse;
import com.nagarro.nagp.urbanclap.ums.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping()
    public ResponseEntity<LoginResponse> loginUser(
            @RequestBody LoginRequest loginRequest
    ) {
        return ResponseEntity.ok(loginService.loginUser(loginRequest));
    }

}
