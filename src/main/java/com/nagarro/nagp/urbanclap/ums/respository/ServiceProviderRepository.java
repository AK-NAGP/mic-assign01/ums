package com.nagarro.nagp.urbanclap.ums.respository;

import com.nagarro.nagp.urbanclap.ums.domain.entity.ServiceProviderEntity;
import com.nagarro.nagp.urbanclap.ums.domain.enums.ServiceTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceProviderRepository extends JpaRepository<ServiceProviderEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM service_provider WHERE SERVICABLE_PINCODES LIKE %:pincode% AND SERVICE_TYPE = :serviceType")
    List<ServiceProviderEntity> findAllByPincodeAndServiceType(@Param("pincode") String pincode, @Param("serviceType") String serviceType);

    List<ServiceProviderEntity> findAllByIsAvailableAndServiceType(Boolean isAvailable, ServiceTypeEnum serviceType);

    @Query(nativeQuery = true, value = "SELECT * FROM service_provider WHERE SERVICABLE_PINCODES LIKE %:pincode% AND IS_AVAILABLE = :isAvailable AND SERVICE_TYPE = :serviceType")
    List<ServiceProviderEntity> findAllByPincodeAndIsAvailableAndServiceType(@Param("pincode") String pincode, @Param("isAvailable") Boolean isAvailable, @Param("serviceType") String serviceType);
}
