package com.nagarro.nagp.urbanclap.ums.domain.model.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginResponse extends BaseResponse{

    private String username;

    private String name;

    private String token;

    public LoginResponse(String status,String message){
        super(status,message);
    }
}
