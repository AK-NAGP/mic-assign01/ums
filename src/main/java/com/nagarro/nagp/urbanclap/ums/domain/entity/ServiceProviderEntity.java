package com.nagarro.nagp.urbanclap.ums.domain.entity;

import com.nagarro.nagp.urbanclap.ums.domain.enums.ServiceTypeEnum;
import com.nagarro.nagp.urbanclap.ums.util.StringListConvertor;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity(name = "service_provider")
@Data
public class ServiceProviderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @Column
    private Boolean isAvailable;

    @Column
    @Enumerated(EnumType.STRING)
    private ServiceTypeEnum serviceType;

    @Column
    @Convert(converter = StringListConvertor.class)
    private List<String> servicablePincodes;
}
