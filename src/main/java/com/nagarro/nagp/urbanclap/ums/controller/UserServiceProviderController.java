package com.nagarro.nagp.urbanclap.ums.controller;

import com.nagarro.nagp.urbanclap.ums.domain.model.request.ServiceProviderRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderListResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderResponse;
import com.nagarro.nagp.urbanclap.ums.service.ServiceProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("service-provider")
public class UserServiceProviderController {

    @Autowired
    private ServiceProviderService providerService;

    @PostMapping("all")
    public ResponseEntity<ServiceProviderListResponse> getAllAvailableProviders(
            @RequestBody ServiceProviderRequest serviceProviderRequest
    ) {
        return ResponseEntity.ok(providerService.getAllAvailableProviders(serviceProviderRequest));
    }


}
