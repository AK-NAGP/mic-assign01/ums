package com.nagarro.nagp.urbanclap.ums.domain.model.response;

import com.nagarro.nagp.urbanclap.ums.domain.enums.ServiceTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceProviderResponse extends BaseResponse {

    private Integer id;

    private String name;

    private String username;

    private String email;

    private String phone;

    private List<String> servicablePincodes;

    private Boolean isAvailable;

    private ServiceTypeEnum serviceType;

    public ServiceProviderResponse(String status,String message){
        super(status, message);
    }
}
