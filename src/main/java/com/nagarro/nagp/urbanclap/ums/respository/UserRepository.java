package com.nagarro.nagp.urbanclap.ums.respository;

import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.enums.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findFirstByUsernameAndPassword(String username, String password);

    Optional<UserEntity> findFirstByIdAndUserType(Integer id, UserType userType);

}
