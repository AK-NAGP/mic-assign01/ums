package com.nagarro.nagp.urbanclap.ums.domain.model.request;

import lombok.Data;

@Data
public class LoginRequest {

    private String username;

    private String password;
}
