package com.nagarro.nagp.urbanclap.ums.respository;

import com.nagarro.nagp.urbanclap.ums.domain.entity.AddressEntity;
import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity,Integer> {

    AddressEntity findByUserEntity(UserEntity userEntity);
}
