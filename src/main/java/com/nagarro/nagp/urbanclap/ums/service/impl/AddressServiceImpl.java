package com.nagarro.nagp.urbanclap.ums.service.impl;

import com.nagarro.nagp.urbanclap.ums.domain.entity.AddressEntity;
import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.UserAddressRequest;
import com.nagarro.nagp.urbanclap.ums.respository.AddressRepository;
import com.nagarro.nagp.urbanclap.ums.service.AddressService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public void saveUserAddress(UserEntity userEntity, UserAddressRequest userAddressRequest) {
        AddressEntity addressEntity = new AddressEntity();

        BeanUtils.copyProperties(userAddressRequest, addressEntity);

        addressEntity.setUserEntity(userEntity);

        addressRepository.save(addressEntity);
    }
}
