package com.nagarro.nagp.urbanclap.ums.jms.consumer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.ServiceProviderRequest;
import com.nagarro.nagp.urbanclap.ums.service.ServiceProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class ServiceProviderStatusConsumer {

    @Autowired
    private ServiceProviderService serviceProviderService;

    @Autowired
    private Gson gson;

    @JmsListener(destination = "uc.ums.serviceProviderStatus")
    public void receiveMessage(String message) {
        ServiceProviderRequest serviceProviderRequest = gson.fromJson(message, ServiceProviderRequest.class);
        serviceProviderService.updateServiceProviderAvailability(
                serviceProviderRequest.getId().toString(),
                serviceProviderRequest.getIsAvailable()
        );
    }
}
