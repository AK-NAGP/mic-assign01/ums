package com.nagarro.nagp.urbanclap.ums.domain.model.request;

import com.nagarro.nagp.urbanclap.ums.domain.enums.UserType;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

@Data
public class SignupRequest extends LoginRequest {

    private String name;

    private UserType userType;

    private String email;

    private String phone;

    //User Only For Service Provider Signup
    private ServiceProviderRequest serviceProvider;

    //User Only For End User Signup
    private UserAddressRequest address;
}
