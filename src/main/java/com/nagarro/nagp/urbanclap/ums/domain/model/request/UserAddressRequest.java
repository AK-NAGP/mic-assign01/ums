package com.nagarro.nagp.urbanclap.ums.domain.model.request;

import lombok.Data;

@Data
public class UserAddressRequest {

    private String addressLine1;

    private String addressLine2;

    private String city;

    private String state;

    private String pincode;

}
