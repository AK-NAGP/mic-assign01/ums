package com.nagarro.nagp.urbanclap.ums.controller;

import com.nagarro.nagp.urbanclap.ums.domain.enums.UserType;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.LoginRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.SignupRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.LoginResponse;
import com.nagarro.nagp.urbanclap.ums.service.SignupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.directory.InvalidAttributesException;

@RestController
@RequestMapping("signup")
public class SignupController {

    @Autowired
    private SignupService signupService;

    @PostMapping
    public ResponseEntity<LoginResponse> loginUser(
            @RequestBody SignupRequest signupRequest
    ) throws InvalidAttributesException {
        if (signupRequest.getUserType().equals(UserType.ADMIN)) {
            throw new InvalidAttributesException("Admin User Cannot Signup");
        }
        return ResponseEntity.ok(signupService.signupUser(signupRequest));
    }
}
