package com.nagarro.nagp.urbanclap.ums.service;

import com.nagarro.nagp.urbanclap.ums.domain.model.request.SignupRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.LoginResponse;

public interface SignupService {

    LoginResponse signupUser(SignupRequest signupRequest);

}
