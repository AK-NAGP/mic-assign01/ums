package com.nagarro.nagp.urbanclap.ums.domain.model.request;

import com.nagarro.nagp.urbanclap.ums.domain.enums.ServiceTypeEnum;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class ServiceProviderRequest {

    private Integer id;

    private Boolean isAvailable;

    //Only User for Searching
    private String pincode;

    private List<String> servicablePincodes;

    private ServiceTypeEnum serviceType;
}
