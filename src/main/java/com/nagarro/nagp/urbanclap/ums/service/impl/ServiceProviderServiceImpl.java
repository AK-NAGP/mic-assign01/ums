package com.nagarro.nagp.urbanclap.ums.service.impl;

import com.google.common.collect.ImmutableList;
import com.nagarro.nagp.urbanclap.ums.domain.entity.ServiceProviderEntity;
import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.ServiceProviderRequest;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.BaseResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderListResponse;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.ServiceProviderResponse;
import com.nagarro.nagp.urbanclap.ums.respository.ServiceProviderRepository;
import com.nagarro.nagp.urbanclap.ums.service.ServiceProviderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ServiceProviderServiceImpl implements ServiceProviderService {

    @Autowired
    private ServiceProviderRepository serviceProviderRepository;

    @Override
    public ServiceProviderListResponse getAllAvailableProviders(ServiceProviderRequest serviceProviderRequest) {
        List<ServiceProviderEntity> serviceProviderEntityList;

        if (Objects.isNull(serviceProviderRequest.getServiceType())) {
            throw new NullPointerException("Service Type cannot be empty");
        }

        if (Objects.nonNull(serviceProviderRequest.getIsAvailable()) && StringUtils.isBlank(serviceProviderRequest.getPincode())) {
            //Area is NOT GIVEN & IsAvailable GIVEN
            serviceProviderEntityList = serviceProviderRepository.findAllByIsAvailableAndServiceType(serviceProviderRequest.getIsAvailable(), serviceProviderRequest.getServiceType());
        } else if (Objects.isNull(serviceProviderRequest.getIsAvailable()) && StringUtils.isNotBlank((serviceProviderRequest.getPincode()))) {
            //Area is GIVEN & IsAvailable NOT GIVEN
            serviceProviderEntityList = serviceProviderRepository.findAllByPincodeAndServiceType(serviceProviderRequest.getPincode(), serviceProviderRequest.getServiceType().name());
        } else if (Objects.nonNull(serviceProviderRequest.getIsAvailable()) && StringUtils.isNotBlank((serviceProviderRequest.getPincode()))) {
            //Area is GIVEN & IsAvailable GIVEN
            serviceProviderEntityList = serviceProviderRepository.findAllByPincodeAndIsAvailableAndServiceType(
                    serviceProviderRequest.getPincode(),
                    serviceProviderRequest.getIsAvailable(),
                    serviceProviderRequest.getServiceType().name());
        } else {
            //Area is NOT GIVEN & IsAvailable NOT GIVEN
            serviceProviderEntityList = serviceProviderRepository.findAll();
        }

        ServiceProviderListResponse serviceProviderListResponse = new ServiceProviderListResponse("success", "All Service Providers");

        List<ServiceProviderResponse> serviceProviders = serviceProviderEntityList
                .stream()
                .map(this::mapEntityToModel)
                .collect(Collectors.toList());

        serviceProviderListResponse.setServiceProviders(serviceProviders);

        return serviceProviderListResponse;
    }

    @Override
    public void signUpUser(UserEntity userEntity, ServiceProviderRequest serviceProvider) {
        ServiceProviderEntity serviceProviderEntity = new ServiceProviderEntity();
        serviceProviderEntity.setUserEntity(userEntity);
        serviceProviderEntity.setServicablePincodes(serviceProvider.getServicablePincodes());
        serviceProviderEntity.setIsAvailable(serviceProvider.getIsAvailable());
        serviceProviderEntity.setServiceType(serviceProvider.getServiceType());

        this.serviceProviderRepository.save(serviceProviderEntity);
    }

    @Override
    public ServiceProviderResponse getUserById(String id) {
        Optional<ServiceProviderEntity> serviceProviderEntityOptional = this.serviceProviderRepository.findById(Integer.parseInt(id));

        return serviceProviderEntityOptional
                .map(this::mapEntityToModel)
                .orElseGet(() -> new ServiceProviderResponse("failure", "Service Provider Not Found"));
    }

    @Override
    public BaseResponse updateServiceProviderAvailability(String userId, Boolean isAvailable) {
        Optional<ServiceProviderEntity> serviceProviderEntityOptional = this.serviceProviderRepository.findById(Integer.parseInt(userId));

        if (serviceProviderEntityOptional.isPresent()) {
            ServiceProviderEntity serviceProviderEntity = serviceProviderEntityOptional.get();
            serviceProviderEntity.setIsAvailable(isAvailable);
            this.serviceProviderRepository.save(serviceProviderEntity);
            return new BaseResponse("success", "Service Provider Updated");
        } else {
            return new BaseResponse("failure", "Service Provider Not Found");
        }

    }

    private ServiceProviderResponse mapEntityToModel(ServiceProviderEntity serviceProviderEntity) {
        ServiceProviderResponse serviceProviderResponse = new ServiceProviderResponse();

        BeanUtils.copyProperties(serviceProviderEntity, serviceProviderResponse);

        serviceProviderResponse.setUsername(serviceProviderEntity.getUserEntity().getUsername());
        serviceProviderResponse.setName(serviceProviderEntity.getUserEntity().getName());
        serviceProviderResponse.setServicablePincodes(serviceProviderEntity.getServicablePincodes());
        serviceProviderResponse.setServiceType(serviceProviderEntity.getServiceType());
        serviceProviderResponse.setEmail(serviceProviderEntity.getUserEntity().getEmail());
        serviceProviderResponse.setPhone(serviceProviderEntity.getUserEntity().getPhone());

        return serviceProviderResponse;
    }
}
