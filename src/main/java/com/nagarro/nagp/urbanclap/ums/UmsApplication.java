package com.nagarro.nagp.urbanclap.ums;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.ums.domain.enums.ServiceTypeEnum;
import com.nagarro.nagp.urbanclap.ums.domain.model.request.ServiceProviderRequest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableJms
public class UmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UmsApplication.class, args);
    }

    @Bean
    public Gson gsonBean() {
        return new Gson();
    }

}
