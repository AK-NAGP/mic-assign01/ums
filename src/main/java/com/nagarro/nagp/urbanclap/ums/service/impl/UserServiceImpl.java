package com.nagarro.nagp.urbanclap.ums.service.impl;

import com.nagarro.nagp.urbanclap.ums.domain.entity.AddressEntity;
import com.nagarro.nagp.urbanclap.ums.domain.entity.UserEntity;
import com.nagarro.nagp.urbanclap.ums.domain.enums.UserType;
import com.nagarro.nagp.urbanclap.ums.domain.model.response.UserResponse;
import com.nagarro.nagp.urbanclap.ums.respository.AddressRepository;
import com.nagarro.nagp.urbanclap.ums.respository.UserRepository;
import com.nagarro.nagp.urbanclap.ums.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public UserResponse findUserByUserId(String userId) {
        Optional<UserEntity> userEntityOptional = userRepository.findFirstByIdAndUserType(
                Integer.parseInt(userId),
                UserType.END_USER
        );

        if(userEntityOptional.isPresent()){
            UserEntity userEntity = userEntityOptional.get();
            AddressEntity addressEntity = addressRepository.findByUserEntity(userEntity);
            UserResponse userResponse = new UserResponse();

            BeanUtils.copyProperties(addressEntity,userResponse);
            BeanUtils.copyProperties(userEntity,userResponse);

            return userResponse;
        }else{
            throw new NullPointerException("User not found");
        }
    }
}
