package com.nagarro.nagp.urbanclap.ums.domain.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceProviderListResponse extends BaseResponse {

    private List<ServiceProviderResponse> serviceProviders;

    public ServiceProviderListResponse(String status, String message) {
        super(status, message);
    }
}
