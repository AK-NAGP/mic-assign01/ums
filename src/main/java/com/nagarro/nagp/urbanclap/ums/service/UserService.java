package com.nagarro.nagp.urbanclap.ums.service;

import com.nagarro.nagp.urbanclap.ums.domain.model.response.UserResponse;

public interface UserService {

    UserResponse findUserByUserId(String userId);
}
