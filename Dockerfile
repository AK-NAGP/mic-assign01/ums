FROM akshatjain244/base-spring-image:latest AS build
MAINTAINER akshat.jain01@nagarro.com

WORKDIR /home/app
ADD pom.xml /home/app
# RUN mvn verify clean --fail-never

COPY . /home/app
RUN mvn -v
RUN mvn clean install -DskipTests

FROM openjdk:8-jdk-alpine
COPY --from=build /home/app/target/ums-0.0.1-SNAPSHOT.jar /home/app/ums-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/home/app/ums-0.0.1-SNAPSHOT.jar"]
EXPOSE 8032